FROM quay.io/openshifthomeroom/workshop-terminal:3.4.2

USER root

RUN HOME=/root && \
    INSTALL_PKGS="skopeo" && \
    yum install -y centos-release-scl epel-release && \
    yum -y --setopt=tsflags=nodocs install --enablerepo=centosplus $INSTALL_PKGS && \
    yum -y clean all --enablerepo='*'

USER 1001

RUN /usr/libexec/s2i/assemble